var net = require('net');

let clientlist = {};

var server = net.createServer(function (connection) {
  console.log('client connected');

  connection.on('error',(error)=>
  {
    console.log(error);
  });

  connection.on('end', function () {
    console.log('client disconnected ' + connection.remotePort);
    delete clientlist[connection.remotePort];
  });

  connection.on('data', function (data) {
   console.log("recv"+data);
   for (var key in clientlist) {
       clientlist[key].write(":_::"+key+":_::"+data);
   }
  });

  clientlist[connection.remotePort] = connection;
  console.log(connection.remotePort);

  for (var key in clientlist) {
    console.log("list " + key);
  }

  connection.write(":?::"+connection.remotePort);
  connection.pipe(connection);


});


server.on('connection', (socket) => {
  console.log("Connect!")
  socket.emit('exception', {errorMessage: 'errorMessage'});
  for (var key in clientlist) {
    clientlist[key].write(":_::"+key+ ":_::connect");
  }
})

server.on('error', () => {
  console.log("error!")
})
console.log("start !!!!!!!!");
server.listen(23456);